import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

# SKIP_TERMS = ["if", "else", "do", ""]

def collect_defines(filepath, lines):
    
    # skip data files    'foo.png.c'
    filename = os.path.basename(filepath)
    if filename.count(".") > 1:
        return

    # print(filepath)
    
    text = "".join(lines)
    clean_lines = i42_refactor.strip_comments_c(text).split("\n")
    del text

    for i, l in enumerate(clean_lines):
        l = l.strip()

        if not l:
            continue

        if 'define' in l:
            if l.lstrip(" #").startswith('define'):
                l = l.split("define", 1)[1]
                l = l.split("(")[0]
                l = l.strip()
                l = l.split(" ")[0]
                l = l.split("\t")[0]
                
                # headers
                if (not l.endswith("_H")) and \
                    (not l.endswith("_H_")) and \
                    (not l.endswith("_H__")) and \
                    (not l.startswith("GL")) and \
                    (not l.startswith("WGL")):
                    defs.add(l)
    return


def word_count_cb(filepath, lines):
    '''
    Callback for 'operate'
    '''
    
    # skip data files    'foo.png.c'
    filename = os.path.basename(filepath)
    if filename.count(".") > 1:
        return
    
    i42_refactor.count_whole_words(filepath, lines,
                                   defs, word_count=defs_users,
                                   strip_comments=True)

defs = set()
i42_refactor.operate("/dsk/data/src/blender/blender/source", collect_defines, i42_refactor.is_c_header)

defs_users = {d: 0 for d in defs}
i42_refactor.operate("/dsk/data/src/blender/blender/source", word_count_cb, i42_refactor.is_c_any, progress=True)

for d, u in sorted(defs_users.items()):
    if u <= 1:
        print(d)
