TAB_SIZE = 4
MAX_LEN = 120

import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

bad_lines = []

def check_skip(l):
    # not arrays
    if l.lstrip().startswith("{"):
        return True

    # only intended lines (not func defs)
    if l[0] not in "\t ":
        return True
    
    return False


def update_funcs(filepath, lines):

    if "datafiles" in filepath:
        return
        
        
    if "wm_" not in filepath:
        return

    for i, l in enumerate(lines):
        le = l.expandtabs(TAB_SIZE)
        if len(le) > MAX_LEN:
            
            if check_skip(l):
                continue
            
            print(le.rstrip())
            print("%s:%d: %d" % (filepath, i + 1, len(le)))
            print()


# i42_refactor.operate("/dsk/data/src/blender/blender/source", update_funcs, i42_refactor.is_c_any)
i42_refactor.operate("/dsk/data/src/blender/blender/source/blender", update_funcs, i42_refactor.is_c_any)
