# collect functiion declarations in headers and find which are never used.

import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import re

import i42_refactor


def collect_functions(filepath, lines):
    print(filepath)


    # skip data files 'foo.png.c'
    filename = os.path.basename(filepath)
    if filename.count(".") > 1:
        return

    text = "".join(lines)
    clean_lines = i42_refactor.strip_comments_c(text).split("\n")
    del text

    # print("\n".join(clean_lines))
    
    
    for i, l in enumerate(clean_lines):
        L = l

        if not l:
            continue

        # ignore anything thats indented
        if l[0].isalpha() == False and l[0] != "_":
            continue

        # quick check this first
        if not ("(" in l):
            continue

        l = l.strip()

        if not l:
            continue
        if l.startswith('typedef'):
            continue
        if "=" in l:
            continue
        if "::" in l:
            continue

        # OK - we got it
        l = l.replace("*", " * ")
        l = l.replace("~", " ~ ")
        l = l.replace("&", " & ")
        l = l.split("(", 1)[0].split()[-1]
        #print(L)
        defs.add(l)
        print(l)
            
    return

def word_count_cb(filepath, lines):
    '''
    Callback for 'operate'
    '''
    
    # skip data files    'foo.png.c'
    filename = os.path.basename(filepath)
    if filename.count(".") > 1:
        return
    
    i42_refactor.count_whole_words(filepath, lines,
                                   defs, word_count=defs_users,
                                   strip_comments=True)

defs = set()
i42_refactor.operate("/dsk/data/src/blender/blender/source", collect_functions, i42_refactor.is_c_header)


defs_users = {d: 0 for d in defs}
i42_refactor.operate("/dsk/data/src/blender/blender/source", word_count_cb, i42_refactor.is_c_any, progress=True)

for d, u in sorted(defs_users.items()):
    if u <= 1:
        print(d)
