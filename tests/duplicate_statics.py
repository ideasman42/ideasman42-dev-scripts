import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

known_types = (
    " void ",
    " char" ,
    " short" ,
    " int" ,
    " long ",
    " float ",
    " double ",
    " signed ",
    " unsigned ",
    " const ",
    " struct ",
)

def collect_statics(filepath, lines):

    # only add once per file
    per_file_defs = set()
    
    # print(filepath)
    for i, l in enumerate(lines):
        # l = l.strip() # no indentation
        l_orig = l

        l = l.rstrip()

        if not l:
            continue
        if l.startswith('//'):
            continue

        if l.startswith("static") and "(" in l:
            
            l = l.replace("\t", " ")

            l = l.partition("static")[2]

            ## Avoid getting this as a func
            # static char str[12 + (MAXPATHLEN * 2)];
            i1 = l.find("(")
            i2 = l.find("[")
            i3 = l.find("{")
            i4 = l.find("=")
            if i2 != -1 and i2 < i1:
                continue
            if i3 != -1 and i3 < i1:
                continue
            if i4 != -1 and i4 < i1:
                continue

            # print(l_orig)

            # get func id
            l_pre = l.partition("(")[0]
            l_pre = " %s " % l_pre
            for t in known_types:
                l_pre = l_pre.replace(t, " ")
            
            l_pre_split = l_pre.split()
            if l_pre_split:
                l_pre = l_pre_split[-1]
                l_pre = l_pre.strip("*()")
                func = l_pre.strip()

                if func not in per_file_defs:
                    static_members.append(func)
                per_file_defs.add(func)


def count_defines(filepath, lines):
    for i, l in enumerate(lines):
        l_split = False
        for d in static_members:
            if d in l:
                if l_split == False:
                    for c in " \t-=+(){}[]*/&^%!~|;:?<>,.'\"":
                        l = l.replace(c, " ")
                    l = l.split()
                    l_split = True

                if d in l:
                    try:
                        static_members_users[d] += 1
                    except:
                        static_members_users[d] = 1

static_members = []
i42_refactor.operate("/dsk/data/src/blender/blender/source", collect_statics, i42_refactor.is_c)

for func in sorted(set(static_members)):
    count = static_members.count(func)
    if count > 1:
        print(func, count)
