import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

# import the replacement list
sys.path.append("/b/source/blender/makesrna/rna_cleanup")
# /b/source/blender/makesrna/rna_cleanup/rna_properties_work.py
import rna_properties_work

# edits = [("", "verts_raw", "vertices_raw")] # ("", "verts", "vertices"), 
edits = []

for prop in rna_properties_work.rna_api:
    class_name = prop[2].split("|")[-1]
    if prop[1] == "changed":
        ok = False
        # if class_name.startswith("Bone"):  ok = True
        # if class_name == "Sculpt": ok = True
        
        #if "Constraint" in class_name: ok = True
        ok = True
        
        if not ok:
            continue

        #if ("pointer" in prop[6] or "string" in prop[6] or "read-only" in prop[7]): # and "_" in prop[3]
        #    continue

        #if not prop[6].startswith("boolean"):
        #    continue
        '''
        if prop[3] in (\
            "disabled", \
            "cyclic", \
            "track", \
            "sticky", \
            "forward", \
            "up", \
            "lock", \
            "axis", \
            "manager", \
            "alt", \
            "oskey", \
            "ctrl", \
            "shift", \
            "debug", \
            "diffuse", \
            "tiles", \
            "layer", \
            "specular", \
            "dither", \
            "jitter", \
            "square", \
            "cubic", \
            "shadows", \
            "shadows", \
            "amount", \
            "display", \
            "velocity", \
            "trand", \
            "parent", \
            "emitter", \
            "viewport", \
            "brightness", \
            "stype", \
            "filter", \
            "mipmap", \
            "range", \
            "lighting", \
            "mist", \
            "stars", \
            "frames", \
            "offset", \
            "cyclic", \
            "alt", \
            "any", \
            "ctrl", \
            "oskey", \
            "shift", \
            "debug", \
            "debug", \
            "lines", \
            "ring", \
            "soft", \
            "star", \
            "texture", \
            "damp", \
            "limit", \
            "invert", \
            "quaternion", \
            "length", \
            "angle", \
            "length", \
            "randomize", \
            "x", \
            "y", \
            "z", \
            "size", \
            "dynamic", \
            "clip", \
            "clip", \
            "children", \
            "normal", \
            "negative", \
            "positive", \
            "mode", \
            "relative", \
            "repeat", \
            "normals", \
            "reversed", \
            "gamma", \
            "bokeh", \
            "relative", \
            "high", \
            "low", \
            "channel", \
            "cutoff", \
            "wrap", \
            "preview", \
            "hue", \
            "sat", \
            "val", \
            "frames", \
            "start", \
            "alpha", \
            "rgb", \
            "fit", \
            "end", \
            "start", \
            "curved", \
            "output_name", \
            "bl_default_closed", \
            "right", \
            "top", \
            "bottom", \
            "left", \
            "negate", \
            "active", \
            "enabled", \
            "still", \
            "falloff", \
            "h", \
            "s", \
            "v", \
            "sun_brightness", \
            "interpolation", \
            "horizon_brightness", \
            ):
            continue
        '''
        edits.append((class_name, prop[3], prop[4]))

for ed in edits:
    print(ed, ",")

1.0 / 0.0

def wash_delimit(line):
    for char in "<>,.;:'\"|\\/~!@#$%^&*()+-=[]{}\n\r\t":
        line = line.replace(char, " ")
    line = " " + line + " "
    line = line.replace("  ", " ")
    return line

def update_funcs_py(filepath, lines):
    # print(filepath)
    for i, l in enumerate(lines):
        for e_class, e_from, e_to in edits:
            if e_from in l:
                l_wash = wash_delimit(l)
                if e_from in l_wash.split():
                    # double check its not a variable name

                    e_from_str = '"%s"' % e_from
                    if e_from_str in l:
                        lines[i] = l.replace(e_from_str, '"%s"' % e_to)

                    e_from_str = "'%s'" % e_from
                    if e_from_str in l:
                        lines[i] = l.replace(e_from_str, "'%s'" % e_to)

                    e_from_str = "." + e_from
                    if e_from_str in l:
                        lines[i] = l.replace(e_from_str, "." + e_to)

                    if l != lines[i]:
                        print(filepath + ":" + str(i), "|", e_from, "|", e_to, "|\n", l[:-1])


classes_str = []
classes = []
for e_class, e_from, e_to in edits:
    classes_str.append('"%s"' % e_class)
    classes.append(e_class)

def update_funcs_c(filepath, lines):
    base = filepath.split("/")[-1]
    '''
    if base.startswith("rna_") and ("actuator" in base or "controller" in base or "sensor" in base): # theme
        class_context = None
        for i, l in enumerate(lines):
            if l.startswith("}"):
                class_context = None
            else:
                for cs in classes_str:
                    if cs in l:
                        class_context = cs[1:-1]

            if "RNA_def_property" in l and "_sdna" not in l:
                for e_class, e_from, e_to in edits:
                    #if e_from == "mode":
                    #    continue
                    if 1: # e_class == class_context:
                        if e_from in l and ('"%s"' % e_from) in l:
                            print(e_class, class_context)
                            print("File " + filepath + ":" + str(i), "|", e_class, "|", e_from, "|", e_to, "|\n", l[:-1])
                            # lines[i] = l.replace(e_from, e_to)
                            break
    '''

    for i, l in enumerate(lines):
        for e_class, e_from, e_to in edits:
            # print(e_from)
            if e_from in l and "(" in l and "_sdna(" not in l:
                e_from_str = '"%s"' % e_from
                if e_from_str in l:
                    print("File " + filepath + ":" + str(i), "|", e_class, "|", e_from, "|", e_to, "|\n", l[:-1])
                    lines[i] = l.replace(e_from_str, ('"%s"' % e_to))
    

    
i42_refactor.operate("/b/release/scripts", update_funcs_py, i42_refactor.is_py)
i42_refactor.operate("/b/source", update_funcs_c, i42_refactor.is_c)
