import re
re_ifndef = re.compile("^\s*#\s*ifndef\s+(.*)$")
re_define = re.compile("^\s*#\s*define\s+(.*)$")

import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

bad_lines = []

def fn_as_guard(fn):
    name = os.path.basename(fn).upper().replace(".", "_").replace("-", "_")
    return "__%s__" % name

old = []

def update_funcs(filepath, lines):

    # first find existing header guard

    found = 0
    def_value = ""
    ok = False
    for i, l in enumerate(lines):
        ndef_match = re_ifndef.match(l)
        if ndef_match:
            ndef_value = ndef_match.group(1).strip()
            for j in range(i + 1, len(lines)):
                l_next = lines[j]
                def_match = re_define.match(l_next)
                if def_match:
                    def_value = def_match.group(1).strip()
                    if def_value == ndef_value:
                        ok = True
                        break
                elif l_next.strip():
                    print(filepath)
                    # found non empty non ndef line. quit
                    break
                else:
                    # allow blank lines
                    pass

            break

    # dud 
    if ok:
        print("found:", def_value, "->", filepath)
        guard = fn_as_guard(filepath)
        
        if def_value != guard:
            old.append(def_value)
            print("%s -> %s" % (def_value, guard))

            # do global regex replace
            pattern = "(?<=[^A-Za-z0-9_])" + def_value + "(?=[^A-Za-z0-9_])"

            re_replace = re.compile(pattern)

            for i, l in enumerate(lines):
                lines[i] = re_replace.sub(guard, l)
    

# i42_refactor.operate("/dsk/data/src/blender/blender/source", update_funcs, i42_refactor.is_c_any)
i42_refactor.operate("/dsk/data/src/blender/blender/source/gameengine", update_funcs, i42_refactor.is_c_header)

print("\|".join([" " + a for a in old]))
