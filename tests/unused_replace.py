warnings = "/data/src/blender/cmake_debug/unused_editors.log"

def extract_line(fn, line):
    return list(open(fn, "r").readlines())[line]

def insert_line(fn, line, line_data):
    data = list(open(fn, "r").readlines())
    data[line] = line_data
    open(fn, "w").writelines(data)


def set_unused(fn, line_n, var):
    line = extract_line(fn, line_n)
    
    # split line in 3 `blah(whee)foo` - we want `wee`
    
    a, b = line.split("(", 1)
    a = a + "("
    b, c = b.rsplit(")", 1)
    c = ")" + c

    # sanitize b
    b = b.replace(",", " , ")
    b = b.replace("*", " * ")
    b_split = b.split()
    for i, b_i in enumerate(b_split):
        if b_i == var:
            b_i = "UNUSED(" + b_i + ")"
            b_split[i] = b_i
    b = " ".join(b_split)
    b = b.replace(" * ", " *")
    b = b.replace(" , ", ", ")
    
    print(a+b+c)
    print(fn)
    print(line_n)
    print(a+b+c)
    insert_line(fn, line_n, a+b+c)

for l in open(warnings, "r"):
    if " warning: unused parameter " in l:
        # get the info...
        # /data/src/blender/blender/source/blender/editors/transform/transform_orientations.c:144:66: warning: unused parameter ‘reports’
        var = l.split()[-1][1:-1]
        fn, line_n, junk = l.split(":", 2)
        line_n = int(line_n) - 1
        # print(fn, line, var)
        
        line = extract_line(fn, line_n)
        
        line_sane = line
        line_sane = line_sane.replace("*", " * ")
        line_sane = line_sane.replace(",", " , ")
        line_sane = " ".join(line.split())
        line_sane = line_sane.replace(" * ", " *")
        line_sane = line_sane.replace(" , ", ", ")

        # check this is an invoke function
        if line_sane.startswith("int ") or line_sane.startswith("static int"):
            i1 = line_sane.find("bContext *")
            if i1 > 0:
                i2 = line_sane.find("wmOperator *")
                if i2 > 0:
                    i3 = line_sane.find("wmEvent *")
                    if i3 > 0:
                        if i1 < i2 < i3:
                            # invoke
                            #print(line_sane)
                            set_unused(fn, line_n, var)
                    else:
                        if i1 < i2:
                            # print(line_sane)
                            set_unused(fn, line_n, var)
                            # exec
        