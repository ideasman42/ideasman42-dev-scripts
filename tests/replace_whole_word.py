import os
import sys
import re

module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor

from_to = (
    ("while", "blah_blah"),
)


def whole_word_pattern(val):
    return "(?<=[^A-Za-z0-9_])" + val + "(?=[^A-Za-z0-9_])"



def update_funcs(filepath, lines):

    str_from = update_funcs.str_from
    str_to = update_funcs.str_to

    pattern =  whole_word_pattern(str_from)

    # do global regex replace
    re_replace = re.compile(pattern)

    for i, l in enumerate(lines):
        lines[i] = re_replace.sub(str_to, l)

update_funcs.str_from = None
update_funcs.str_to = None


for str_from, str_to in from_to:
    update_funcs.str_from = str_from
    update_funcs.str_to = str_to
    i42_refactor.operate("/dsk/data/src/blender/blender/source", update_funcs, i42_refactor.is_c_any)


#i42_refactor.operate("/b/release/scripts/addons_contrib", update_funcs, i42_refactor.is_py)
#i42_refactor.operate("/b/release/scripts/addons", update_funcs, i42_refactor.is_py)
#i42_refactor.operate("/b/release/scripts/addons", update_funcs, i42_refactor.is_py)
#i42_refactor.operate("/dsk/data/src/blender/blender/release/scripts/startup/bl_ui/", update_funcs, i42_refactor.is_py)
