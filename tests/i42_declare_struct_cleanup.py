
import os
import sys
module_dir = os.path.join(os.path.dirname(__file__), "..", "modules")
print(module_dir)
sys.path.append(module_dir)

import i42_refactor


def cmake_from_source(f):
    d = os.path.dirname(f)
    while 1:
        f = os.path.join(d, "CMakeLists.txt")
        if os.path.exists(f):
            return f
        d = os.path.dirname(d)

# ./test.sh 2>&1 | grep MEM | sort | cut -d: -f1 | uniq

import re

# struct Foo; --> Foo
re_struct = re.compile(r"\s*struct\s+([a-zA-Z0-9_]+)\s*;")

def update_funcs(filepath, lines):
    structs = set()
    lines_nonstruct = []
    for i, l in enumerate(lines):
        m = re_struct.match(l)
        if m is not None:
            structs.add(m.group(1))
        else:
            lines_nonstruct.append(l)

    data = "".join(lines_nonstruct)

    structs_remove = set()

    for s in sorted(structs):
        if not re.search(r"\b" + s + r"\b", data):
            structs_remove.add(s)

    i = len(lines)
    while i > 0:
        i -= 1
        l = lines[i]

        m = re_struct.match(l)
        if m is not None:
            if m.group(1) in structs_remove:
                del lines[i]

    '''
    if structs_remove:
        print(filepath)
        print(structs_remove)
    '''


i42_refactor.operate("/src/blender/source/blender", update_funcs, lambda a: a.endswith((".h", ".hpp", ".hxx")))
# i42_refactor.operate("/src/blender", update_funcs, lambda a: a.endswith("CMakeLists.txt"))
# i42_refactor.operate("/src/blender_manual_migration", update_funcs, lambda a: a.endswith(".rst"))

