# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
# in a new file...
import i42_refactor
def update_funcs(filepath, lines):
    # example adding am extra argument
    for i, l in enumerate(lines):
        if "RNA_def_property_pointer_funcs" in l and "const char *poll" not in l:
            lines[i] = l.replace(");", ", NULL);")

i42_refactor.operate("/b/", update_funcs, i42_refactor.is_c)
"""

import os
from os.path import splitext


# extension checking
def is_c_header(filename):
    ext = splitext(filename)[1]
    return (ext in (".h", ".hpp", ".hxx"))

def is_c(filename):
    ext = splitext(filename)[1]
    return (ext in (".c", ".cpp", ".cxx"))

def is_c_any(filename):
    return is_c(filename) or is_c_header(filename)

def is_cmake(filename):
    ext = splitext(filename)[1]
    return (ext == ".cmake") or (filename == "CMakeLists.txt")

def is_scons(filename):
    ext = splitext(filename)[1]
    return (ext == ".py" or filename in ("SConscript", "SConstruct"))

def is_py(filename):
    ext = splitext(filename)[1]
    return (ext == ".py")

def source_list(path, filename_check=None):
    for dirpath, dirnames, filenames in os.walk(path):

        # skip '.svn'
        if dirpath.startswith("."):
            continue

        for filename in filenames:
            if filename_check is None or filename_check(filename):
                yield os.path.join(dirpath, filename)

# utility functions
def whitespace_get(line):
    return line[:len(line) - len(line.lstrip())]


# operate on a file
def file_to_lines(filepath):
    try:
        return open(filepath, "r", encoding='utf8').readlines()
    except UnicodeDecodeError:
        print("Error loading:", filepath)
        import traceback
        traceback.print_exc()

def lines_to_file(filepath, lines):
    open(filepath, "w", encoding='utf8').writelines(lines)

def operate(root, line_editor, filename_check, progress=False):
    source_filepaths = list(source_list(root, filename_check=filename_check))
    source_filepaths_tot = len(source_filepaths)

    for i, f  in enumerate(source_filepaths):
        lines = file_to_lines(f)

        if lines is None:
            continue

        lines_back = lines[:]

        line_editor(f, lines)

        if lines != lines_back:
            lines_to_file(f, lines)
            
        if progress:
            print("\tprogress: %.2f%% - %r" % ((i / source_filepaths_tot) * 100, f))

# -----------------
# Comment Stripping
# - C only atm

# http://stackoverflow.com/questions/241327/python-snippet-to-remove-c-and-c-comments
def strip_comments_c(text):
    import re

    def replacer(match):
        s = match.group(0)
        if s.startswith('/'):
            return ""
        else:
            return s
    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )
    return re.sub(pattern, replacer, text)
    

# --------
# Whole word count

def translate_whole_words(txt):
    
    # generated like this
    
    return txt.translate({  # control characters 0x0-0x1f
                            # 0x00: "_",
                          0x01: "_",
                          0x02: "_",
                          0x03: "_",
                          0x04: "_",
                          0x05: "_",
                          0x06: "_",
                          0x07: "_",
                          0x08: "_",
                          0x09: "_",
                          0x0a: "_",
                          0x0b: "_",
                          0x0c: "_",
                          0x0d: "_",
                          0x0e: "_",
                          0x0f: "_",
                          0x10: "_",
                          0x11: "_",
                          0x12: "_",
                          0x13: "_",
                          0x14: "_",
                          0x15: "_",
                          0x16: "_",
                          0x17: "_",
                          0x18: "_",
                          0x19: "_",
                          0x1a: "_",
                          0x1b: "_",
                          0x1c: "_",
                          0x1d: "_",
                          0x1e: "_",
                          0x1f: "_",

                          0x7f: "_",  # 127

                          # for i in " \t-=+(){}[]*/&^%!~|;:?<>,.'\"": print("%s: ' ',  # %s" % (hex(ord(i)), i))
                          0x2d: ' ',  # -
                          0x3d: ' ',  # =
                          0x2b: ' ',  # +
                          0x28: ' ',  # (
                          0x29: ' ',  # )
                          0x7b: ' ',  # {
                          0x7d: ' ',  # }
                          0x5b: ' ',  # [
                          0x5d: ' ',  # ]
                          0x2a: ' ',  # *
                          0x2f: ' ',  # /
                          0x26: ' ',  # &
                          0x5e: ' ',  # ^
                          0x25: ' ',  # %
                          0x21: ' ',  # !
                          0x7e: ' ',  # ~
                          0x7c: ' ',  # |
                          0x3b: ' ',  # ;
                          0x3a: ' ',  # :
                          0x3f: ' ',  # ?
                          0x3c: ' ',  # <
                          0x3e: ' ',  # >
                          0x2c: ' ',  # ,
                          0x2e: ' ',  # .
                          0x27: ' ',  # '
                          0x22: ' ',  # "

                          })


def count_whole_words(filepath, lines, words, word_count=None, strip_comments=True):
    '''
    Count whole 'words' - which must be a sequence of strings.
    
    word_count - must be a dict, where the key is the word and the value is accumulated.
    if if arg isnt passed a new dict will be created.
    
    returns the word_count dict.
    '''

    if word_count is None:
        word_count = {}

    text = " ".join(lines)

    if strip_comments:
        text = strip_comments_c(text)

    # get whole words, (stupid)
    text = translate_whole_words(text)

    text_words = text.split()

    #for w in text_words:
    #    print(w)

    for d in words:
        c = text_words.count(d)
        if c:
            try:
                word_count[d] += c
            except:
                word_count[d] = c
