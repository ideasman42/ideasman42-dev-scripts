# This is a module to manage windows from python.
# for more desktop info: xprop -root

class _wm_ctrl:
    """ Fake module, store all `wmctrl` calls here
    """
    import os

    @staticmethod
    def wm_list():
        import subprocess

        try:
            process = subprocess.Popen(["wmctrl", "-u", "-p", "-G", "-x", "-l"],
                                       stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except OSError:
            import traceback
            traceback.print_exc()
            return []

        data = process.stdout.read()
        
        data_ls = []
        for l in data.split(b"\n"):
            l = l.decode("utf-8", "replace")
            # print(l)
            l = l.split(" ")

            # last line
            if len(l) <= 1:
                continue

            # clumsy, find a better way?
            l_ok = []
            l_title = []
            for word in l:
                if len(l_ok) != 9:
                    if word:
                        l_ok.append(word)
                else:
                    l_title.append(word)
            l = l_ok
            title = " ".join(l_title)

            data_ls.append(dict(uid=int(l[0], base=16),
                                gravity=int(l[1]),
                                pid=int(l[2]),
                                x=int(l[3]),
                                y=int(l[4]),
                                w=int(l[5]),
                                h=int(l[6]),
                                wmclass=l[7],
                                hostname=l[8],
                                title=title,
                                ))
        return data_ls
    
    @staticmethod
    def window_uid_get(uid):
        for w in _wm_ctrl.wm_list():
            if w["uid"] == uid:
                return w
        return None

    @staticmethod
    def window_uid_exists(uid):
        return (uid in (w["uid"] for w in _wm_ctrl.wm_list()))

    @staticmethod
    def window_title_set(uid, title, is_long=True):
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-r", str(uid),
                                    "-N" if is_long else "-I", repr(title)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def window_desktop_set(uid, desktop):
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-r", str(uid),
                                    "-t", repr(desktop)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def window_mvarg_set(uid, gravity=-1, x=-1, y=-1, w=-1, h=-1):
        """ -1 on any of the args means do nothing"""
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-r", str(uid),
                                    "-e", "%d,%d,%d,%d" % (gravity, x, y, w, h)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def window_prop_set(uid, prop, val):
        assert(isinstance(val, bool))
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-r", str(uid),
                                    "-b", "%s,%s" % ("add" if val else "remove", prop)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def window_activate(uid):
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-a", str(uid)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def window_close(uid):
        import subprocess
        process = subprocess.Popen(["wmctrl", "-i", "-c", str(uid)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

    @staticmethod
    def desktop_set(desktop):
        import subprocess
        process = subprocess.Popen(["wmctrl",
                                    "-s", repr(desktop)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        process.stdout.read()

class _xprop:

    # internal use only
    @staticmethod
    def _window_get_id(uid, prefix):
        import subprocess
        process = subprocess.Popen(["xprop", "-id", repr(uid)],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        data = process.stdout.read()
        for l in data.split(b"\n"):
            if l.startswith(prefix):
                l = l.decode("utf-8")
                if prefix.endswith(b"(WINDOW)"):
                    return l.split(" window id # ", 1)[1].strip()
                else:
                    return l.split(" = ", 1)[1].strip()
        return None
    @staticmethod
    def _root_get_id(prefix):
        import subprocess
        process = subprocess.Popen(["xprop", "-root"],
                                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        data = process.stdout.read()
        for l in data.split(b"\n"):
            if l.startswith(prefix):
                l = l.decode("utf-8")
                if prefix.endswith(b"(WINDOW)"):
                    return l.split(" window id # ", 1)[1].strip()
                else:
                    return l.split(" = ", 1)[1].strip()
        return None

    @staticmethod
    def window_active_id():
        data = _xprop._root_get_id(b"_NET_ACTIVE_WINDOW(WINDOW)")
        if data is not None:
            return int(data, base=16)
        return int(0, base=16)

    @staticmethod
    def window_desktop_get(uid):
        data = _xprop._window_get_id(uid, b"_NET_WM_DESKTOP(CARDINAL)")
        if data is not None:
            return int(data)
        return 0

    @staticmethod
    def window_title_icon_get(uid):
        data = _xprop._window_get_id(uid, b"_NET_WM_VISIBLE_ICON_NAME(UTF8_STRING)")
        if data is not None:
            return data[1:-1]  # strip quotes
        return 0

    # check properties
    @staticmethod
    def window_prop_is_fullscreen(uid):
        data = _xprop._window_get_id(uid, b"_NET_WM_STATE(ATOM)")
        if data is not None:
            return "_NET_WM_STATE_FULLSCREEN" in data.split(", ")
        assert(0)
    @staticmethod
    def window_prop_is_maximized_vert(uid):
        data = _xprop._window_get_id(uid, b"_NET_WM_STATE(ATOM)")
        if data is not None:
            return "_NET_WM_STATE_MAXIMIZED_VERT" in data.split(", ")
        assert(0)
    def window_prop_is_maximized_horz(uid):
        data = _xprop._window_get_id(uid, b"_NET_WM_STATE(ATOM)")
        if data is not None:
            return "_NET_WM_STATE_MAXIMIZED_HORZ" in data.split(", ")
        assert(0)

    @staticmethod
    def desktop_active_id():
        data = _xprop._root_get_id(b"_NET_CURRENT_DESKTOP(CARDINAL)")
        if data is not None:
            return int(data)
        return 0

# TODO
# _NET_DESKTOP_NAMES(UTF8_STRING) = "one", "two", "three"


class Window():
    __slots__ = ("_uid",
                 )

    def __init__(self, uid):
        self._uid = uid

    def __eq__(self, other):
        return self._uid == other._uid
    def __ne__(self, other):
        return self._uid != other._uid

    def exists(self):
        return _wm_ctrl.window_uid_exists(self._uid)

    @property
    def title(self):
        assert(self.exists())
        return _wm_ctrl.window_uid_get(self._uid)["title"]
    @title.setter
    def title(self, title):
        assert(isinstance(title, str))
        _wm_ctrl.window_title_set(self._uid, title, is_long=True)

    @property
    def title_icon(self):
        assert(self.exists())
        # return _wm_ctrl.window_uid_get(self._uid)["title"]
        return _xprop.window_title_icon_get(self._uid)
    @title_icon.setter
    def title_icon(self, title):
        assert(isinstance(title, str))
        _wm_ctrl.window_title_set(self._uid, title, is_long=False)

    @property
    def id(self):
        assert(self.exists())
        return self._uid

    @property
    def wmclass(self):
        assert(self.exists())
        return _wm_ctrl.window_uid_get(self._uid)["wmclass"]

    @property
    def hostname(self):
        assert(self.exists())
        return _wm_ctrl.window_uid_get(self._uid)["hostname"]

    @property
    def pid(self):
        assert(self.exists())
        return _wm_ctrl.window_uid_get(self._uid)["pid"]

    @property
    def size(self):
        assert(self.exists())
        data = _wm_ctrl.window_uid_get(self._uid)
        return (data["w"], data["h"])
    @size.setter
    def size(self, size):
        assert(self.exists())
        assert(isinstance(title, str))
        _wm_ctrl.window_title_long_set(self._uid, title)

    @property
    def desktop(self):
        assert(self.exists())
        return _xprop.window_desktop_get(self._uid)
    @desktop.setter
    def desktop(self, desktop):
        assert(self.exists())
        assert(isinstance(desktop, int))
        _wm_ctrl.window_desktop_set(self._uid, desktop)

    @property
    def gravity(self):
        assert(self.exists())
        data = _wm_ctrl.window_uid_get(self._uid)
        return (data["w"], data["h"])
    @gravity.setter
    def gravity(self, gravity):
        assert(self.exists())
        assert(isinstance(title, int))
        _wm_ctrl.window_mvarg_set(self._uid, gravity=gravity)

    @property
    def modal(self):
        assert(self.exists())
        return None
    @modal.setter
    def modal(self, val):
        assert(self.exists())
        assert(isinstance(val, bool))
        _wm_ctrl.window_prop_set(self._uid, "modal", val)

    @property
    def fullscreen(self):
        assert(self.exists())
        return _xprop.window_prop_is_fullscreen(self._uid)
    @fullscreen.setter
    def fullscreen(self, val):
        assert(self.exists())
        assert(isinstance(val, bool))
        _wm_ctrl.window_prop_set(self._uid, "fullscreen", val)

    @property
    def maximized_vert(self):
        assert(self.exists())
        return _xprop.window_prop_is_maximized_vert(self._uid)
    @maximized_vert.setter
    def maximized_vert(self, val):
        assert(self.exists())
        assert(isinstance(val, bool))
        _wm_ctrl.window_prop_set(self._uid, "maximized_vert", val)

    @property
    def maximized_horz(self):
        assert(self.exists())
        return _xprop.window_prop_is_maximized_horz(self._uid)
    @maximized_horz.setter
    def maximized_horz(self, val):
        assert(self.exists())
        assert(isinstance(val, bool))
        _wm_ctrl.window_prop_set(self._uid, "maximized_horz", val)

    '''
    # properties
    _props = ("modal",
              "sticky",
              "maximized_vert", "maximized_horz",
              "shaded",
              "skip_taskbar", "skip_pager",
              "hidden",
              "fullscreen",
              "above", "below")
    '''


    # -------
    # Methods
    def activate(self):
        _wm_ctrl.window_activate(self._uid)

    def close(self):
        _wm_ctrl.window_close(self._uid)


class WM:

    @property
    def windows(self):
        return [Window(w["uid"]) for w in _wm_ctrl.wm_list()]

    def window_active():
        uid = _xprop.window_active_id()
        return Window(uid) if uid else None
        
    @property
    def desktop(self):
        assert(self.exists())
        return _xprop.desktop_active_id()
    @desktop.setter
    def desktop(self, desktop):
        assert(self.exists())
        assert(isinstance(desktop, int))
        _wm_ctrl.desktop_set(desktop)
# _NET_WM_STATE(ATOM) = _NET_WM_STATE_FULLSCREEN

# --
wm = WM()

for w in wm.windows:
    #if "scite" not in w.wmclass.lower():
    #    w.close()
    #w.title = "Hello"
    #print(w.wmclass)
    # a = w.title
    #print("%r" % w.title)
    print(w.maximized_horz, w.maximized_vert)
    w.maximized_horz, w.maximized_vert = True, True
    # w.fullscreen = False
    print(w.title_icon)
