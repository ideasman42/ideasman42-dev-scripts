# git checkout -b 2.1 origin/2.1

cd /dsk/src
if [ -f qt-creator ]
then
	git clone git://gitorious.org/qt-creator/qt-creator.git
	cd qt-creator
else
	cd qt-creator
	git reset --hard HEAD
	git pull  --no-edit
	git reset --hard HEAD
	# git clean -f -d -x
fi

# patch -p1 < version.diff

# qmake -r CONFIG+=debug
qmake -r

# mkdir -p /dsk/data/src/qt-creator/share/doc/qtcreator
# touch    /dsk/data/src/qt-creator/share/doc/qtcreator/qtcreator.qch
# touch    /dsk/data/src/qt-creator/share/doc/qtcreator/qtcreator-dev.qch

make INSTALL_ROOT=/opt/qtc -j6
make INSTALL_ROOT=/opt/qtc install -k
