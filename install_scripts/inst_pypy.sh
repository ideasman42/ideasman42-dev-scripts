cd /dsk/src/pypy
hg pull
hg up

cd /dsk/src/pypy/pypy/goal
python2 ../../rpython/bin/rpython --opt=jit targetpypystandalone.py

mkdir -p /opt/pypy/bin
ln -s /dsk/src/pypy/pypy/translator/goal/pypy-c /opt/pypy/bin/
