
SRCDIR="/dsk/src"
INSTDIR="/opt/ninja"

if [ -d $SRCDIR/ninja/.git ] ; then
	echo "Updating $SRCDIR/ninja"
	cd $SRCDIR/ninja
	git pull
else
	echo "Checking out "$SRCDIR"/ninja ..."
	git clone https://github.com/martine/ninja.git $SRCDIR/ninja
	cd $SRCDIR/ninja
fi

CFLAGS=-O3 python2 ./configure.py
python2 ./bootstrap.py

rm ~/bin/ninja
ln -s $SRCDIR/ninja/ninja ~/bin/ninja
