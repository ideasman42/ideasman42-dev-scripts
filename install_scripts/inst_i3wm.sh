#!/bin/sh
cd /dsk/src
if [ -f i3 ]; then
	git clone https://github.com/i3/i3
	cd i3
else
	cd i3
	# git reset --hard HEAD
	git pull  --no-edit
	# git reset --hard HEAD
	# git clean -f -d -x
fi

if [ -f ../i3_build ]; then
	mkdir ../i3_build
fi

autoreconf -fi
cd ../i3_build
../i3/configure --prefix=/opt/i3
make -j8 install

