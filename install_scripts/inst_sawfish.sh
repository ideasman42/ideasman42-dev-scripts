#!/bin/sh
 
SRCDIR="/dsk/src"
INSTDIR="/opt/sawfish"
MAKEARGS="-j6"

if [ -d $SRCDIR/sawfish ] ; then
	echo "Updating "$SRCDIR"/sawfish ..."
	cd $SRCDIR/sawfish
	cd ./sawfish  && git reset --hard HEAD && git pull && cd -
	cd ./librep   && git reset --hard HEAD && git pull && cd -
	cd ./repgtk   && git reset --hard HEAD && git pull && cd -
else
	echo "Checking out "$SRCDIR"/sawfish ..."
	mkdir $SRCDIR/sawfish
	cd $SRCDIR/sawfish
	
	git clone git://github.com/SawfishWM/sawfish.git sawfish
	git clone git://github.com/SawfishWM/librep.git  librep
	git clone git://github.com/SawfishWM/rep-gtk.git repgtk
fi

# so 'rep' command can be found
export PATH=$PATH:$INSTDIR/bin

# LIBREP
cd ./librep
./autogen.sh --prefix=$INSTDIR
make         $MAKEARGS
make install $MAKEARGS


# REPGTK
cd ..
cd repgtk
PKG_CONFIG_PATH=$INSTDIR/lib/pkgconfig ./autogen.sh --prefix=$INSTDIR
make         $MAKEARGS
make install $MAKEARGS


# SAWFISH
cd ..
cd sawfish
PKG_CONFIG_PATH=$INSTDIR/lib/pkgconfig ./autogen.sh --prefix=$INSTDIR
make         $MAKEARGS
make install $MAKEARGS
