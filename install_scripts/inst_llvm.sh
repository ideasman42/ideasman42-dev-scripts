#!/bin/sh

# eek
export LD_LIBRARY_PATH=/opt/gcc/lib64

SRCDIR="/dsk/src"
INSTDIR="/opt/llvm"

echo ""

if [ -d $SRCDIR/llvm/.svn ] ; then
	echo "Updating $SRCDIR/llvm $SRCDIR/llvm/tools/clang $SRCDIR/llvm/projects/compiler-rt"
	svn cleanup $SRCDIR/llvm $SRCDIR/llvm/tools/clang $SRCDIR/llvm/projects/compiler-rt
	svn up $SRCDIR/llvm $SRCDIR/llvm/tools/clang $SRCDIR/llvm/projects/compiler-rt
else
	echo "Checking out "$SRCDIR"/llvm ..."
	svn checkout http://llvm.org/svn/llvm-project/llvm/trunk $SRCDIR/llvm
	svn checkout http://llvm.org/svn/llvm-project/cfe/trunk $SRCDIR/llvm/tools/clang
	svn checkout http://llvm.org/svn/llvm-project/compiler-rt/trunk $SRCDIR/llvm/projects/compiler-rt
fi


rm -rf $INSTDIR

mkdir $SRCDIR/llvm_cmake
cd $SRCDIR/llvm_cmake
#~ mkdir $SRCDIR/llvm_build
#~ cd $SRCDIR/llvm_build


echo ""
echo "Building..."


cmake ../llvm \
	-DCMAKE_INSTALL_PREFIX:STRING=$INSTDIR \
	-DCMAKE_BUILD_TYPE:STRING=Release \
	-DLLVM_ENABLE_THREADS:BOOL=ON \
	-DLLVM_TARGETS_TO_BUILD:STRING="CppBackend;X86" \
	-DPYTHON_EXECUTABLE=/usr/bin/python2

#~ ../llvm/configure \
	#~ --prefix=/opt/llvm \
	#~ --enable-optimized \
	#~ --disable-docs


# make -j3

echo ""
echo "Installing..."

make -j6 install

cp $SRCDIR/llvm/tools/clang/tools/scan-view/scan-view $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-view/*.py $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-build/scan-build $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-build/ccc-analyzer $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-build/c++-analyzer $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-build/sorttable.js $INSTDIR/bin/
cp $SRCDIR/llvm/tools/clang/tools/scan-build/scanview.css $INSTDIR/bin/

echo ""
echo "Done!"
