cd /dsk/src/

svn revert -R opencollada-read-only
svn up opencollada-read-only
cd opencollada-read-only
svn_clean.sh

cd ../opencollada-build/
cmake .
make -j6
