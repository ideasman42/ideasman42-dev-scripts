cd /dsk/src/pathscale/compiler
git pull

cd /dsk/src/pathscale/compiler_build
cmake   -DCMAKE_INSTALL_PREFIX="/opt/path64" \
        -DCMAKE_BUILD_TYPE=Release \
        .

make
make install

