
cd /dsk/src
if [ -d gcc ]; then
	echo "Checking out svn"
	svn checkout svn://gcc.gnu.org/svn/gcc/trunk gcc
else
	echo "Updating svn"
	svn up gcc/
fi

cd gcc

mkdir ../gcc_build
cd ../gcc_build

echo "Configure..."

../gcc/configure \
  --prefix=/opt/gcc \
  --enable-stage1-languages=c,c++,lto \
  --enable-languages=c,c++,lto \
  --disable-multilib \
  --with-system-zlib \
  --disable-bootstrap \


# --enable-shared \



echo "Build..."

make -j6
make install

# replace own gcc
rm /usr/bin/c++
rm /usr/bin/cpp
rm /usr/bin/g++
rm /usr/bin/gcc
rm /usr/bin/gcc-ar
rm /usr/bin/gcc-nm
rm /usr/bin/gcc-ranlib
rm /usr/bin/gcov

ln -s /opt/gcc/bin/c++            /usr/bin
ln -s /opt/gcc/bin/cpp            /usr/bin
ln -s /opt/gcc/bin/g++            /usr/bin
ln -s /opt/gcc/bin/gcc            /usr/bin
ln -s /opt/gcc/bin/gcc-ar         /usr/bin
ln -s /opt/gcc/bin/gcc-nm         /usr/bin
ln -s /opt/gcc/bin/gcc-ranlib     /usr/bin
ln -s /opt/gcc/bin/gcov           /usr/bin


