cd /dsk/src/SDL
hg pull
hg up

cd /dsk/src/SDL_build
../SDL/configure --prefix=/opt/sdl13
make -j5
make install
