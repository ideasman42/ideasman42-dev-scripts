#!/usr/bin/env python

COMPILER = '/usr/bin/gcc'

SPLINT = "/opt/splint/bin/splint"
CPPCHECK = "/root/bin/cppcheck"

VERBOSE = False

import sys
import os


def extract_gcc_info(argv, use_cxx=False):
    """ Generic function to extract source/includes/defines
    """
    l = argv
    l = [f.replace("-isystem", "-I") for f in l]

    c_files = [f for f in l if f.lower().endswith('.c')]

    if use_cxx:
        c_files += [f for f in l if f.lower().endswith('.cxx')]
        c_files += [f for f in l if f.lower().endswith('.cpp')]

    inc_dirs = [f[2:].strip() for f in l if f.startswith('-I')]
    defs = [f[2:].strip() for f in l if f.startswith('-D')]
    return c_files, inc_dirs, defs


def run_splint():
    SPLINT_ARGS = ("-weak "
                   "-posix-lib "
                   "-linelen 10000  "
                   "+ignorequals "
                   "+relaxtypes "
                   "-retvalother "
                   "+matchanyintegral "
                   "+longintegral "
                   "+ignoresigns "
                   "-nestcomment "
                   "-predboolothers "
                   "-ifempty "
                   "-unrecogcomments "

                   # We may want to remove these later
                   "-type "
                   "-fixedformalarray "
                   "-fullinitblock "
                   "-fcnuse "
                   "-initallelements "
                   "-castfcnptr "

                   # re-definitions, generated code causes most of these
                   "-redef "

                   # These can be useful but clutter a lot
                   "-bufferoverflowhigh "
                   "-unrecog "
                   "-varuse "

                   # these can be useful but clutter too
                   #"-syntax "
                   #"-namechecks "
                   )


    c_files, inc_dirs, defs = extract_gcc_info(sys.argv[1:])

    for c in c_files:
        cmd = '%s %s %s %s %s' % (SPLINT,
                                  SPLINT_ARGS,
                                  c,
                                  " ".join(["-I" + w for w in inc_dirs]),
                                  " ".join(["-D" + w for w in defs])
                                  )

        if VERBOSE:
            print(cmd)
        os.system(cmd)

def run_cppcheck():
    CPPCHECK_ARGS = ("--enable=all "
                     "-I/usr/include "
                     )

    c_files, inc_dirs, defs = extract_gcc_info(sys.argv[1:], use_cxx=True)

    for c in c_files:
        cmd = '%s %s %s %s %s' % (CPPCHECK,
                                  CPPCHECK_ARGS,
                                  c,
                                  " ".join(["-I" + w for w in inc_dirs]),
                                  " ".join(["-D" + w for w in defs])
                                  )

        if VERBOSE:
            print(cmd)
        os.system(cmd)


def run_gcc():
    cmd = "%s %s" % (COMPILER, "-Werror " + " ".join(sys.argv[1:]))
    
    
    #cmd = "%s %s" % ("strace /opt/ekopath-4.0.10/bin/pathcc", " ".join([w for w in sys.argv[1:] if not w.startswith("-W") and w != "-O0"] + ["-Ofast"]))
    if VERBOSE:
        print(cmd)
    os.system(cmd)


def dump_py():
    c_files, inc_dirs, defs = extract_gcc_info(sys.argv[1:], use_cxx=True)
    print("DUMP_PY: %s," % str((c_files, inc_dirs, defs)))

if __name__ == "__main__":
    #run_splint()
    #run_cppcheck()
    run_gcc()
    #dump_py()
    

'''
cat err.txt | grep -v "Finished checking" | grep -v "Splint 3\.1\.2" | grep -v "^$" | grep -v "The scope of the variable" | grep -v "is assigned a value that is never used" | grep -v "Preprocessing error for file" | grep -v " Cannot continue" | grep -v "Parse Error"  | grep -v "Cppcheck cannot find all the include files" | grep -v "Please report bug to" | grep -v " syntax error" | grep -v "attempting to continue, results may be incorrect" | grep -v " Internal Bug at " | grep -v "(error)"

'''

