#!/usr/bin/python
# gcc_splint.py
#
# First capture the output of building blender to get all the C files with their includes
# make VERBOSE=1 > log.txt

# Run cppcheck on every C file...
# python gcc_splint.py log.txt



import os
import sys


def is_c_header(filename):
    ext = os.path.splitext(filename)[1]
    return (ext in (".h", ".hpp", ".hxx"))


def is_c_header(filename):
    ext = os.path.splitext(filename)[1]
    return (ext in (".h", ".hpp", ".hxx"))


def is_c(filename):
    ext = os.path.splitext(filename)[1]
    return (ext in (".c", ".cpp", ".cxx", ".m", ".mm", ".rc"))


def is_c_any(filename):
    return os.path.s_c(filename) or is_c_header(filename)


def build_info_from_makelog(compiler, filepath):
    makelog = open(filepath, 'r')
    source = []
    for line in makelog:

        args = line.split()

        if compiler not in args:
            continue

        # join args incase they are not.
        args = ' '.join(args)
        args = args.replace(" -D ", "-D")
        args = args.replace(" -I ", "-I")
        args = args.split()
        # end

        # remove compiler
        args[:args.index(compiler) + 1] = []

        c_files = [f for f in args if is_c(f)]
        inc_dirs = [f[2:].strip() for f in args if f.startswith('-I')]
        defs = [f[2:].strip() for f in args if f.startswith('-D')]
        for c in sorted(c_files):
            source.append((c, inc_dirs, defs))

    return source


def queue_commands(commands, processes=1):
    import time
    import subprocess
    processes = []
    for cmd in commands:
        # wait until a thread is free
        while 1:
            processes[:] = [p for p in processes if p.poll() is None]

            if len(processes) < THREADS:
                break
            else:
                time.sleep(0.1)

        processes.append(subprocess.Popen(cmd))


def cpp_check():
    THREADS = 6
    COMPILER = '/usr/bin/gcc'
    OUT = 'cppcheck_warn.txt'

    CPPCHECK = "cppcheck"
    CPPCHECK_ARGS = []
    # ---
    
    import sys
    argv = sys.argv

    source_info = build_info_from_makelog(COMPILER, sys.argv[-1])

    cppcheck_commands = []
    for c, inc_dirs, defs in source_info:
        cmd = ([CPPCHECK] +
                CPPCHECK_ARGS +
               [c] +
               [("-I%s" % i) for i in inc_dirs] +
               [("-D%s" % d) for d in defs]
              )

        # print(" ".join(cmd))

        cppcheck_commands.append(cmd)

    queue_commands(cppcheck_commands, processes=THREADS)


def all_cpp():
    COMPILER = '/usr/bin/gcc'
    # ---

    source_info = build_info_from_makelog(COMPILER, sys.argv[-1])
    all_c_source = set()
    all_inc = set()
    all_defs = set()
    for c, inc_dirs, defs in source_info:
        all_c_source.add(c)
        all_inc.update(set(inc_dirs))
        all_defs.update(set(defs))
    
    all_inc = [("-I" + i) for i in sorted(list(all_inc))]
    all_defs = [("-D" + i) for i in sorted(list(all_defs))]

    print("gcc -C -E " + " " + " ".join(all_inc) + " " + " ".join(all_defs))

if __name__ == "__main__":
    # main()
    all_cpp()
