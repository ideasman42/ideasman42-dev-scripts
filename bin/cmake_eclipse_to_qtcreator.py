#! /usr/bin/env python3

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

# by Campbell Barton

# Assume this runs from the build dir where the eclipse project files are
# Tested with CMake 2.8.5 and QtCreator 2.3

import os
from os.path import join, splitext
import sys

# -----------------------------------------------------------------------------
# Parse Eclipse


def parse_project(PROJECT_DIR):

    values = {}

    from xml.dom.minidom import parse
    tree = parse(join(PROJECT_DIR, ".project"))

    ELEMENT_NODE = tree.ELEMENT_NODE

    descr, = tree.getElementsByTagName("projectDescription")
    name = descr.getElementsByTagName("name")[0].firstChild.nodeValue
    values["name"] = name

    location = ""
    # now find the source dir
    for link_res in descr.getElementsByTagName("linkedResources"):
        for link in link_res.getElementsByTagName("link"):
            name = link.getElementsByTagName("name")[0].firstChild.nodeValue
            if name == "[Source directory]":
                location = link.getElementsByTagName("location")[0].firstChild.nodeValue

    values["location"] = location

    return values


def parse_cproject(PROJECT_DIR):

    defines = []
    includes = []
    source = []

    from xml.dom.minidom import parse
    tree = parse(join(PROJECT_DIR, ".cproject"))

    ELEMENT_NODE = tree.ELEMENT_NODE

    cproject, = tree.getElementsByTagName("cproject")
    for storage in cproject.childNodes:
        if storage.nodeType != ELEMENT_NODE:
            continue
        if not storage.hasAttribute("moduleId"):
            continue

        if storage.attributes["moduleId"].value == "org.eclipse.cdt.core.settings":
            cconfig = storage.getElementsByTagName("cconfiguration")[0]
            for substorage in cconfig.childNodes:
                if substorage.nodeType != ELEMENT_NODE:
                    continue

                moduleId = substorage.attributes["moduleId"].value

                if moduleId == "org.eclipse.cdt.core.pathentry":
                    for path in substorage.childNodes:
                        if path.nodeType != ELEMENT_NODE:
                            continue
                        kind = path.attributes["kind"].value

                        if kind == "mac":
                            # <pathentry kind="mac" name="PREFIX" path="" value="&quot;/opt/blender25&quot;"/>
                            defines.append((path.attributes["name"].value, path.attributes["value"].value))
                        elif kind == "inc":
                            # <pathentry include="/data/src/blender/blender/source/blender/editors/include" kind="inc" path="" system="true"/>
                            includes.append(path.attributes["include"].value)
                        else:
                            pass

                elif moduleId == "org.eclipse.cdt.make.core.buildtargets":
                    for target_collection in substorage.getElementsByTagName("buildTargets"):
                        for target in target_collection.childNodes:
                            if target.nodeType == ELEMENT_NODE and target.hasAttribute("path"):
                                path = target.attributes["path"].value
                                # normally only 1
                                for target_member in target.getElementsByTagName("buildTarget"):
                                    # again, only 1
                                    for target_member_child in target_member.childNodes:
                                        value = target_member_child.nodeValue
                                        if value[-2:] in (".o", ".i", ".s"):
                                            source.append(join(path, value[:-2]))

    source[:] = sorted(set(source))

    values = {
        "includes": includes,  # absolute
        "defines": defines,
        "source": source,      # relative
        }

    return values


# -----------------------------------------------------------------------------
# Utility Functions


def cmake_cache_var(PROJECT_DIR, var):
    cache_file = open(join(PROJECT_DIR, "CMakeCache.txt"))
    lines = [l_strip for l in cache_file for l_strip in (l.strip(),) if l_strip if not l_strip.startswith("//") if not l_strip.startswith("#")]
    cache_file.close()

    for l in lines:
        if l.split(":")[0] == var:
            return l.split("=", 1)[-1]
    return None


def cmake_compiler_defines(PROJECT_DIR):
    compiler = cmake_cache_var(PROJECT_DIR, "CMAKE_C_COMPILER")  # could do CXX too

    if compiler is None:
        print("Couldn't find the compiler, os defines will be omitted...")
        return

    import tempfile
    temp_c = tempfile.mkstemp(suffix=".c")[1]
    temp_def = tempfile.mkstemp(suffix=".def")[1]

    os.system("%s -dM -E %s > %s" % (compiler, temp_c, temp_def))

    temp_def_file = open(temp_def)
    lines = [l.strip() for l in temp_def_file if l.strip()]
    temp_def_file.close()

    os.remove(temp_c)
    os.remove(temp_def)
    return lines


def is_c_header(filename):
    ext = splitext(filename)[1]
    return (ext in {".h", ".hpp", ".hxx", ".hh"})


def is_c(filename):
    ext = splitext(filename)[1]
    return (ext in {".c", ".cpp", ".cxx", ".m", ".mm", ".rc", ".cc", ".inl"})


def is_c_any(filename):
    return is_c(filename) or is_c_header(filename)


def source_list(path, filename_check=None):
    for dirpath, dirnames, filenames in os.walk(path):

        # skip '.svn'
        if dirpath.startswith("."):
            continue

        for filename in filenames:
            filepath = join(dirpath, filename)
            if filename_check is None or filename_check(filepath):
                yield filepath


# -----------------------------------------------------------------------------
# QtCreator Project file spesific code


def qtcreator_generic_project(project_values,
                              cproject_values,
                              PROJECT_DIR,
                              use_all=False,
                              use_compiler_defines=True,
                              ):

    # Blender-Debug@cmake_debug --> Blender
    PROJECT_NAME = project_values["name"].split("@", 1)[0].rsplit("-", 1)[0].lower()
    SOURCE_DIR = project_values["location"]

    print("")
    print("Generating project files for: %r" % PROJECT_NAME)
    print("")
    print("  --all=%r" % use_all)
    print("  --with-comp-defs=%r" % use_compiler_defines)
    print("")
    print("  Build Dir: %r" % PROJECT_DIR)
    print("  Source Dir: %r" % SOURCE_DIR)
    print("")

    source = [join(SOURCE_DIR, f) for f in cproject_values["source"]]
    if use_all:
        source += list(sorted(source_list(SOURCE_DIR, filename_check=is_c_any)))
    else:
        # hrmf, headers are not included by eclipse, scan for them instead
        source += list(sorted(source_list(SOURCE_DIR, filename_check=is_c_header)))
    source[:] = sorted(set(source))

    f = open(join(PROJECT_DIR, "%s.files" % PROJECT_NAME), 'w')
    f.write("\n".join(source))

    f = open(join(PROJECT_DIR, "%s.includes" % PROJECT_NAME), 'w')
    f.write("\n".join([f for f in cproject_values["includes"]]))

    qtc_prj = join(PROJECT_DIR, "%s.creator" % PROJECT_NAME)
    f = open(qtc_prj, 'w')
    f.write("[General]\n")

    qtc_cfg = join(PROJECT_DIR, "%s.config" % PROJECT_NAME)
    f = open(qtc_cfg, 'w')
    f.write("// ADD PREDEFINED MACROS HERE!\n")
    defines_final = [("#define %s %s" % item) for item in cproject_values["defines"]]
    if use_compiler_defines:
        if sys.platform[:3] != "win":
            defines_final += cmake_compiler_defines(PROJECT_DIR)  # defines from the compiler
    f.write("\n".join(defines_final))

    print("project file written to %r" % qtc_prj)
    print("")


def main():
    args = sys.argv[1:]

    if "--help" in args:
        print("QtCreator Project File Generator:\n"
              "  %r [build_dir] [options]\n"
              "\n"
              "  --all: Use all source files found in the source dir\n"
              "         by recursively globbing."
              "\n"
              "  --with-comp-defs: Use compiler defines\n" %
              os.path.basename(__file__))
        return

    PROJECT_DIR = os.getcwd()

    if args:
        for path in args:
            if os.path.exists(path):
                PROJECT_DIR = path

    if not (os.path.exists(join(PROJECT_DIR, ".cproject")) and os.path.exists(join(PROJECT_DIR, ".project"))):
        print("Error, missing eclipse prject files (.project & .cproject)\n"
              "in path: %r\n"
              "Run %r from the build directory or spesify the path as an argument.\n"
              "Use cmake to generate Eclipse project files, eg:\n"
              "    cmake %r -G\"Eclipse CDT4 - Unix Makefiles\"\n"
              "\n"
              "aborting.\n" % (PROJECT_DIR, os.path.basename(__file__), PROJECT_DIR))
        return

    project_values = parse_project(PROJECT_DIR)
    cproject_values = parse_cproject(PROJECT_DIR)
    qtcreator_generic_project(project_values,
                              cproject_values,
                              PROJECT_DIR,
                              use_all=("--all" in args),
                              use_compiler_defines=("--with-comp-defs" not in args),
                              )


if __name__ == "__main__":
    main()