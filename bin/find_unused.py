import os

def is_ignore(v):
    if v.startswith(b'_'):
        return True
    if v.startswith(b'zLhm6'):
        return True
    if v.startswith(b'AUD_'):
        return True
    return False

def main():
    import subprocess
    out = subprocess.check_output(["objdump", "--syms", "/dsk/data/src/blender/blender/blender.bin"])
    out = out.split(b"\n")
    out.sort()
    vars = set()
    for v in out:
        if b'g     F .text\t' in v:
            v = v.split()[-1]
            if is_ignore(v):
                continue
            vars.add(v)
    
    func = {}

    for i, v in enumerate(sorted(vars)):
        v = v.decode("ASCII")
        
        args = [ \
                "grep",
                "-R",
                "-E",
                "'(^|[^a-zA-Z])%s($|[^a-zA-Z])'" % v,
                "--include",
                "'*.c'",
                "*",
            ]

        if 1:
            #out = subprocess.check_output(args)
            os.system(" ".join(args) + " 1> /tmp/foo.txt")
            out = [l for l in open("/tmp/foo.txt").readlines() if l.strip()]
            
        else:
            os.system(" ".join(args) + " > /tmp/foo.txt" )
            out = [l for l in open("/tmp/foo.txt").readlines() if l.strip()]
        
        c = func[v] = len(out)
        
        if c == 1:
            print("%.2f" % ((i / len(vars)) * 100.0), c , v)

        #if i > 100:
        #    break

        #print(out)
        
        # local!
        #if b'l     F .text\t' in v:
    print("\n\nCounts:")
    func_ls = [(c, v) for v, c in func.items()]
    func_ls.sort()
    for c, v in func_ls:
        if c < 2:
            print(v, c)
    
    
    
    #print(out[1:100])
    # 

main()