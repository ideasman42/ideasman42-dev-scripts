#!/usr/bin/python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import os
import sys
import tempfile
from os.path import join, dirname


def main():

    if len(sys.argv) == 1:
        print 'Give a dir to search through, no action taken (aborting).'
        return

    EDITOR = os.environ.get("EDITOR")
    if EDITOR is None:
        print("Environment variable $EDITOR. was not set (aborting).")
        return

    file_ls = []

    argv = sys.argv[1:]

    i = 0
    while i+1 < len(argv):
        if argv[i].endswith('\\'):
            argv[i] = argv[i][ :-1] + argv[i]
            argv.pop(i+1)
        else:
            i+=1

    cwd =  os.getcwd()

    for dir in argv:
        dir = os.path.abspath(dir)

        # ROOT, DIRS, FILES
        path_walker = os.walk(dir)
        path_ls = [True]

        while True:
            try:
                path_ls = path_walker.next()
                for file in path_ls[2]:
                    path = path_ls[0]
                    file_ls.append(join(path, file))
            except:
                break

    if not file_ls:
        print 'No files found, no action taken.'
        return

    #filename = os.tmpnam()
    filename = tempfile.mktemp()


    file = open(filename, 'w')

    for l in file_ls:
        if '\n' in l[:-1]:
            print 'Newline found in filename "%s", aborting, no action taken' % l
            return
            

    for l in file_ls:
        file.write('%s\n' % l.split(os.sep)[-1])
    file.close()

    os.system('$EDITOR "%s"' % filename)
    # os.system('scite %s' % filename)

    file = open(filename, 'r')
    lines_edited = [l.rstrip() for l in file.readlines()]
    file.close()
    
    if len(lines_edited) != len(file_ls):
        print 'Edited rename file has different number of lines. aborting, no action taken'
        os.remove(filename)
        return

    renamedCount = 0
    nonRenamedCount = 0
    errorCount = 0
    for orig, new in zip(file_ls, lines_edited):
        new = join(dirname(orig), new)
        if new == orig:
            print 'not renaming "%s"' % orig
            nonRenamedCount +=1
        else:
            try:
                os.rename(orig, new)
                renamedCount +=1
                print '\trenaming "%s" to "%s"' % (orig, new)
            except:
                print '\terror renaming "%s" to "%s"' % (orig, new)
                errorCount +=1

    os.remove(filename)

    print '*** renamed %i files, %i untouched, %i errors. ***' % (renamedCount, nonRenamedCount, errorCount)


if __name__ == '__main__':
    main()
