import os
import subprocess
# /src/blender/source/blender/blenlib/intern/BLI_array.c
# /src/blender/source/blender/editors/sculpt_paint/paint_image.c
DIR = "/src/blender/source/blender/editors"
#DIR = "/b/source/blender/editors"
#DIR = "/dsk/data/src/blender/bmesh"
#DIR = "/b/source/blender/python"
DIR_BUILD = "/src/cmake_debug/source/blender/editors"
COMMENT = "//(INCLUDE_LINT)"
TMP_OUT = "/tmp/build_err.txt"

def build():
    proc = subprocess.Popen("make -C %s" % DIR_BUILD,
                            shell=True,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            )
    stdout_value, stderr_value = proc.communicate()
    print(stderr_value)
    return stderr_value, proc.returncode


def comment(filename, line):
    file = open(filename, "r", encoding="utf-8")
    lines = file.readlines()
    file.close()
    
    lines[line] = COMMENT + lines[line]

    file = open(filename, "w", encoding="utf-8")
    file.writelines(lines)

    return lines[line]


def uncomment(filename, line):
    file = open(filename, "r", encoding="utf-8")
    lines = file.readlines()
    file.close()


    lines[line] = lines[line][len(COMMENT):]

    file = open(filename, "w", encoding="utf-8")
    file.writelines(lines)

    return lines[line]


def test_include(include):
    #if '"IMB_' in include or '"MEM_' in include or '"ED_' in include or '"BPY_' in include or '"GPU_' in include or '"BIF_' in include or '"GEN_' in include or '"WM_' in include: # or '"BLI_' in include
    #if "BKE_pointcache" in include:
    #    return True
    #return True
    if "BLI_rand" in include:
        return True
    return False


def remove(filename, line):
    file = open(filename, "r", encoding="utf-8")
    lines = file.readlines()
    file.close()

    del lines[line]

    file = open(filename, "w", encoding="utf-8")
    file.writelines(lines)


import os
from os.path import splitext

def header_list(path):
    for dirpath, dirnames, filenames in os.walk(path):

        # skip '.svn'
        if dirpath.startswith("."):
            continue

        for filename in filenames:
            print(filename)
            ext = splitext(filename)[1]
            if ext in (".c", ".cpp", ".cxx"):
                yield os.path.join(dirpath, filename)


def main():
    files = list(header_list(DIR))
    files.sort()
    print(files)
    
    for f in files:
        
        #if "/outliner" not in f:
        #    continue
        #if "editderivedmesh.c" not in f:
        #    continue

        print("checking: %s" % f)
        
        includes = []

        file = open(f, 'r', encoding="utf-8")
        for i, l in enumerate(file):
            l = l.lstrip()
            if l.startswith("#") and l[1:].lstrip().startswith("include"):
                if test_include(l):
                    includes.append(i)
        file.close()

        # better to remove last. first, incase removing a header at the start only
        # complains because of a later header which uses it but is not needed.
        includes.reverse()

        if includes:
            print("    found: %s dummy build" % len(includes))
            os.utime(f)
            out_orig, returncode = build()

            if returncode != 0:
                print("*************************************\n"
                      "Dummy build has non zero return code!\n"
                      "*************************************")
                includes = []

            for i in includes:

                # annoying, need this so cmake rebuilds
                # need to look into a better way to ensyre a file is rebuilt.
                import time
                time.sleep(1)

                line = comment(f, i)
                print("   test build without %d" % i)
                out_test, returncode = build()
                if returncode != 0 or out_test != out_orig:
                    uncomment(f, i)
                    print("kept %s:%d (%s)" % (f, i, line))
                else:
                    print("removed %s:%d (%s)" % (f, i, line))
                    remove(f, i)

            build()

if __name__ == "__main__":
    main()
