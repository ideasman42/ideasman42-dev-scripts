from pygments import highlight, lex
from pygments.lexers import CLexer
from pygments.formatters import RawTokenFormatter

from pygments.token import Token



global filepath
tokens = []

# could store index here too, then have prev/next methods
class TokStore:
    __slots__ = ("type", "text", "line")
    def __init__(self, type, text, line):
        self.type = type
        self.text = text
        self.line = line


def tk_range_to_str(a, b):
    return "".join([tokens[i].text for i in range(a, b + 1)])

def tk_item_is_newline(tok):
    return tok.type == Token.Text and tok.text == "\n"

def tk_item_is_ws_newline(tok):
    return (tok.type == Token.Text and tok.text.isspace()) or \
           (tok.type in Token.Comment)

def tk_item_is_ws(tok):
    return (tok.type == Token.Text and tok.text != "\n" and tok.text.isspace()) or \
           (tok.type in Token.Comment)

# also skips comments
def tk_advance_ws(index, direction):
    while tk_item_is_ws(tokens[index + direction]) and index > 0:
        index += direction
    return index

def tk_advance_ws_newline(index, direction):
    while tk_item_is_ws_newline(tokens[index + direction]) and index > 0:
        index += direction
    return index + direction

def tk_match_backet(index):
    backet_start = tokens[index].text
    assert(tokens[index].type == Token.Punctuation)
    assert(backet_start in "[]{}()")
    
    if tokens[index].text in "({[":
        direction = 1
        backet_end = {"(": ")", "[": "]", "{": "}"}[backet_start]
    else:
        direction = -1
        backet_end = {")": "(", "]": "[", "}": "{"}[backet_start]

    level = 1
    index_match = index + direction
    while True:
        item = tokens[index_match]
        if item.type == Token.Punctuation:
            if item.text == backet_start:
                level += 1
            elif item.text == backet_end:
                level -= 1
                if level == 0:
                    break

        index_match += direction

    return index_match


def tk_index_is_linestart(index):
    index_prev = tk_advance_ws_newline(index, -1)
    return tokens[index_prev].line < tokens[index].line


def extract_statement_if(index_kw):
    # assert(tokens[index_kw].text == "if")

    # seek back
    i = index_kw

    i_start = tk_advance_ws(index_kw - 1, direction=-1)
    
    # seek forward
    i_next = tk_advance_ws_newline(index_kw, direction=1)
    
    #print(tokens[i_next])
    
    if tokens[i_next].type != Token.Punctuation or tokens[i_next].text != "(":
        print("Error line: %d" % tokens[index_kw].line)
        print("if (\n"
              "   ^\n"
              ""
              "Character not found, insetad found:")
        print(tk_range_to_str(i_start, i_next))
        return None

    i_end = tk_match_backet(i_next)
    
    return (i_start, i_end)


def warning(message, index_kw_start, index_kw_end):
    print("%s:%d: warning: %s" % (filepath, tokens[index_kw_start].line, message))
    # print(tk_range_to_str(index_kw_start, index_kw_end))


# ------------------------------------------------------------------
# Own Blender rules here!

def blender_check_kw_if(index_kw_start, index_kw, index_kw_end):

    # check if we have: 'if('
    if not tk_item_is_ws(tokens[index_kw + 1]):
        warning("no white space between 'if('", index_kw_start, index_kw_end)

    # check for: if (a
    #                && b)
    # note: prefer to keep operators at the end of the line
    if tokens[index_kw].line != tokens[index_kw_end].line:
        # need to loop over all tokens
        for i in range(index_kw, index_kw_end):
            # && or ||
            if (tokens[i + 0].type == Token.Operator and tokens[i + 0].text in {"&", "|"} and
                tokens[i + 1].type == Token.Operator and tokens[i + 1].text in {"&", "|"} and
                tokens[i + 0].text == tokens[i + 1].text):

                if tk_index_is_linestart(i):
                    warning("if operator starts a new line 'if (\\n&& ...)\\n{'", index_kw_start, index_kw_end)

    # check for: ){
    index_next = tk_advance_ws_newline(index_kw_end, 1)
    if tokens[index_next].type == Token.Punctuation and tokens[index_next].text == "{":
        if not tk_item_is_ws(tokens[index_kw + 1]):
            warning("no white space between trailing bracket 'if (){'", index_kw_start, index_kw_end)
            
        # check for: if ()
        #            {
        # note: if the if statement is multi-line we allow it
        if ((tokens[index_kw].line == tokens[index_kw_end].line) and
            (tokens[index_kw].line == tokens[index_next].line - 1)):

           warning("if body brace on a new line 'if ()\\n{'", index_kw_start, index_kw_end)
        
        

def scan_source(fp):
    # print("scanning: %r" % fp)

    global filepath

    filepath = fp

    #print(highlight(code, CLexer(), RawTokenFormatter()).decode('utf-8'))
    code = open(filepath, 'r', encoding="utf-8").read()

    tokens[:] = []
    line = 1

    for ttype, text in lex(code, CLexer()):
        tokens.append(TokStore(ttype, text, line))
        line += text.count("\n")


    for i, tok in enumerate(tokens):
        if tok.type == Token.Keyword:
            if tok.text in {"if", "for", "while", "switch"}:
                item_range = extract_statement_if(i)
                blender_check_kw_if(item_range[0], i, item_range[1])

        #print(ttype, type(ttype))
        #print((ttype, value))
        
    #for ttype, value in la:
    #    #print(value, end="")


def scan_source_recursive(dirpath):
    import os
    from os.path import join, splitext

    def source_list(path, filename_check=None):
        for dirpath, dirnames, filenames in os.walk(path):

            # skip '.svn'
            if dirpath.startswith("."):
                continue

            for filename in filenames:
                filepath = join(dirpath, filename)
                if filename_check is None or filename_check(filepath):
                    yield filepath

    def is_source(filename):
        ext = splitext(filename)[1]
        return (ext in {".c", ".inl", ".cpp", ".cxx", ".hpp", ".hxx", ".h"})

    for filepath in source_list(dirpath, is_source):
        if "datafiles" in filepath:
            continue

        scan_source(filepath)


if __name__ == "__main__":
    scan_source_recursive("/dsk/data/src/blender/blender/source/blender/editors")
    '''
    for filepath in sys.argv[1:]:
        if os.path.isdir(filepath):
            # recursive search
            scan_source_recursive(filepath)
        else:
            # single file
            scan_source(filepath)

    '''